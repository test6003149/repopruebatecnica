<?php

namespace App\Http\Controllers;

use App\Models\User; // Asegúrate de importar el modelo User si aún no está importado
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all(); // Obtener todos los usuarios desde la base de datos

        return view('users.index', ['users' => $users]);
    }
}
