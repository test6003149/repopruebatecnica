<!-- resources/views/users/index.blade.php -->

<h1>Listado de Usuarios</h1>

<ul id="user-list">
    @foreach($users as $user)
        <li>{{ $user->name }}</li>
    @endforeach
</ul>

